<img src="/doc/pics/GPK_BME_MOGI.png">

# Behaviour Model

## Dependencies

**Turtlebot3** <br>
**Turtlebot3_msgs** <br>
[**Mecanumbot_core**](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot_core) - The core code of the mecanumbot <br>
[**MecanumBot**](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot) - Mid level functionalities of the MecanumBot, control nodes.  <br>
[**MocapROS**](https://gitlab.com/microlab1/private-projects/etorobotics/mocapros) package is mandatory to use this node. The package provide the stream from the Mocap system.<br>
 
    $ sudo apt install ros-noetic-turtlebot3-msgs
    $ sudo apt install ros-noetic-turtlebot3
    $ cd ~/catkin_ws/src
    $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot_core.git
    $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot_core.git
    $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mocapros.git
    $ cd ..
    $ catkin_make    

## Install

To use the code make sure the dependendcies are installed properly. Clone the git repository:

    $ cd ~/catkin_ws/src
    $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/behaviourmodel.git
    $ cd ..
    $ catkin_make

## Bringup  

Start the `roscore`:

    [PC-Terminal]: $ roscore

Start the stream of the Mocap and start the mocap stream:

    [PC-Terminal]: $ rosrun mocapstream stream_publisher.py 

Bring up the robot:

    [PI-Terminal]: $ roslaunch mecanumbot_core mecanumbot_robot.launch

Start control nodes:

    [PC-Terminal]: $ roslaunch mecanum mecanum_control.launch

For following the owner run:

    [PC-Terminal]: $ rosrun ros_behaviourmodel follow_owner.py

For the more complex behaviour run (this node is under development):

    [PC-Terminal]: $ rosrun ros_behaviourmodel start_behaviourmodel.py

## References

- **Internal Git Repos:**

[Etofilter](https://gitlab.com/microlab1/private-projects/etorobotics/etofilter) - The Measurement software with GUI <br>
[Mocapy](https://gitlab.com/microlab1/private-projects/etorobotics/mocapy) - The Python kliens code for the Mocap Stream <br>
[Ainsworth Data Analysis](https://gitlab.com/microlab1/private-projects/etorobotics/a15_ainsworthdataanalysis) - Solomon Coder config and help. Basic Neural Network predictions. <br>

- **External Data Storage**

[Measurements](https://drive.google.com/drive/folders/1lQeEloyUibZBLYOo_FJfhy4wZXmpqf2c?usp=sharing) - Google Driver folder containing the measurements (.mp4, .csv, .ecsv)

## Authors and acknowledgment
Benjámin Krivácsy <br>
Balázs Nagy (email: nagybalazs@mogi.bme.hu) <br>

## TODO

## Task list

- [x] Video convert AVi to MP4 (Balász)
- [x] Readmek frissítése (Balázs)
- [x] Témavezetőt megkérdezni (Balázs)
- [x] Solomon decode (Beni)
- [ ] Solomon decode (Balázs)
- [ ] Jupyter notebook (Beni)
  - [x] Adatbeolvasás
    - [x] Preprocess (szürés, megfeleltetés)
  - [x] Transforme háló implementáció
  - [x] Háló tanítás
- [x] Működő robot koncepció (Balázs + Dávid)
- [x] Attractor modell vs. costmap
- [x] Különböző transzformer hálók (egy tulajdonságra vs. több kimenet) / kb. 80% várható pontosság
- [x] Külön mérésen tesztelni
- [x] Launch file-t készíteni (stream, mecanum.launch)
