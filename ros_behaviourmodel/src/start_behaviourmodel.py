#!/usr/bin/env python3

from dataprocess import *
from robot_behaviour import *
from neural_models import *
from movement import *
from StreamVector import *
import argparse

import rospy
from mocapstream.msg import StreamVector


streamVector = StreamVector_template()


# subscriber functions
def update_StreamVector(msg):
    streamVector.data = msg.data


def start_behaviourmodel():
    # init
    init_done = False
    movement = Movement()
    neural = Neural()

    # Construct the argument parser
    ap = argparse.ArgumentParser()
    ap.add_argument("-t", "--type", required=False, help="Type of the behaviour model.")
    args = vars(ap.parse_args())

    if args['type'] is not None:
        robBehaviour = RobotBehaviour(int(args['type']))
    else:
        robBehaviour = RobotBehaviour(0)

    rate = rospy.Rate(10)
    rospy.loginfo("Behaviourmodel node started.")

    while not rospy.is_shutdown():  # run the node until Ctrl-C is pressed
        # connection to mocapy stream for moving
        rospy.Subscriber("stream", StreamVector, update_StreamVector, queue_size=10)
        if not init_done:
            # call the dataProcessor and calculate the necessary data
            data = dataProcessor_StreamVector(streamVector.data)
            init_done = True
        else:
            # calculate the new data
            data.calculate_data(streamVector.data)
            neural.predict_tail_wagging(data)

            robBehaviour.update_based_on_data(data)
            robBehaviour.update_behaviour(data)

            data.grabber_closed = movement.goto_point(data, robBehaviour)

        rate.sleep()

    rospy.loginfo("Behaviourmodel node stopped.")


if __name__ == "__main__":
    rospy.init_node("start_behaviourmodel")
    start_behaviourmodel()
