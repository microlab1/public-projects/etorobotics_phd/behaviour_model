#!/usr/bin/env python3

from math import *
import rospy
from geometry_msgs.msg import Twist
from mecanum_grabber.srv import ControlGrabber


class Movement:
    def __init__(self):
        self.yaw_threshold_rot = 3
        self.yaw_threshold_forward = 25
        self.dist_threshold = 0.4
        self.toy_dist_threshold = 0.23

        self.control_linear_x_vel = 0.0
        self.control_linear_y_vel = 0.0
        self.control_angular_vel = 0.0

        self.twist = Twist()
        self.pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)

        rospy.wait_for_service("control_grabber")
        self.control_client = rospy.ServiceProxy("control_grabber", ControlGrabber)

        self.grabber_closed = False
        self.grabber(False)

        self.get_toy = False

        rospy.loginfo("Movement node initialized.")

    def goto_point(self, data, robBehaviour):
        # reset variables
        self.control_angular_vel = 0.0
        self.control_linear_x_vel = 0.0
        self.control_linear_y_vel = 0.0

        dest_array = robBehaviour.dest

        x = dest_array[0]
        z = dest_array[1]

        if data.toy.pos.x == x and data.toy.pos.z == z:
            self.get_toy = True
        else:
            self.get_toy = False

        dist = sqrt(pow((x - data.robot.pos.x), 2) + pow((z - data.robot.pos.z), 2))
        dist_x = (x - data.robot.pos.x)
        dist_z = (z - data.robot.pos.z)

        offset = 0.0
        rot = 0.0

        if dist_z < 0:
            offset = 90
        elif dist_z > 0:
            offset = - 90
        else:
            offset = 0

        if dist_x == 0 and dist_z == 0:
            self.twist.linear.x = self.control_linear_x_vel
            self.twist.linear.y = self.control_linear_y_vel
            self.twist.linear.z = 0.0

            self.twist.angular.x = 0.0
            self.twist.angular.y = 0.0
            self.twist.angular.z = self.control_angular_vel

            self.pub.publish(self.twist)

            return self.grabber_closed
        else:
            rot_xz = atan(dist_x / dist_z) * 180 / pi

        rot = rot_xz - data.robot.orientation.euler.yaw + offset

        if rot > 180:
            rot = -360 + rot
        elif rot < -180:
            rot = 360 + rot
        if rot < 0:
            self.control_angular_vel = -0.3
        elif rot > 0:
            self.control_angular_vel = 0.3
        else:
            self.control_angular_vel = 0.0

        if abs(rot) <= self.yaw_threshold_forward:
            if dist <= self.dist_threshold:
                if self.get_toy:
                    if dist <= self.toy_dist_threshold:
                        self.grabber(True)
                    else:
                        self.control_linear_x_vel = 0.08
                else:
                    if data.grabber_closed:
                        self.grabber(False)
                    self.get_toy = False
                    self.control_linear_x_vel = 0.0
            else:
                self.control_linear_x_vel = 0.08
            if abs(rot) <= self.yaw_threshold_rot:
                self.control_angular_vel = 0.0

        self.twist.linear.x = self.control_linear_x_vel
        self.twist.linear.y = self.control_linear_y_vel
        self.twist.linear.z = 0.0

        self.twist.angular.x = 0.0
        self.twist.angular.y = 0.0
        self.twist.angular.z = self.control_angular_vel

        self.pub.publish(self.twist)

        return self.grabber_closed

    def grabber(self, close_grabber):
        # the two statement is separated, so further calculations can be made
        if close_grabber:
            status = self.control_client(close_grabber)
            if status:
                self.grabber_closed = True
        else:
            status = self.control_client(close_grabber)
            if status:
                self.grabber_closed = False
        return
