#!/usr/bin/env python3

import rospy
import time
from math import *

# Scipy
try:
    from scipy.spatial.transform import Rotation as R
except:
    print("Warning: Unable to import Scipy --> Quaternion-to-Euler and Euler-to-Quaternion conversions cannot be made!")

from mocapstream.msg import StreamVector
from geometry_msgs.msg import Twist

# global variable
streamVector = StreamVector()


# subscriber functions
def update_StreamVector(msg):
    streamVector.data = msg.data


def follow_owner():
    # init variables
    twist = Twist()

    owner_tracked = False
    robot_tracked = False

    yaw_threshold = 5
    dist_thershold = 0.70

    control_linear_x_vel = 0.0
    control_linear_y_vel = 0.0
    control_angular_vel = 0.0

    count = 0

    pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
    rate = rospy.Rate(100)

    rospy.loginfo("Follow owner node started.")

    while not rospy.is_shutdown():  # run the node until Ctrl-C is pressed
        # connection to mocapy stream for moving
        rospy.Subscriber("stream", StreamVector, update_StreamVector, queue_size=10)

        owner_tracked = False
        robot_tracked = False

        # follow the  owner
        for obj in streamVector.data:
            if obj.name == "OWN":
                owner = obj
                owner_tracked = True

                euler = R.from_quat([owner.orientation.quat.q1, owner.orientation.quat.q2, owner.orientation.quat.q3,
                                     owner.orientation.quat.q4]).as_euler("yxz", degrees=degrees)
                owner.orientation.euler.pitch = euler[1]
                owner.orientation.euler.yaw = euler[0]
                owner.orientation.euler.roll = euler[2]

            elif obj.name == "ROB":
                robot = obj
                robot_tracked = True

                euler = R.from_quat([robot.orientation.quat.q1, robot.orientation.quat.q2, robot.orientation.quat.q3,
                                     robot.orientation.quat.q4]).as_euler("yxz", degrees=degrees)
                robot.orientation.euler.pitch = euler[1]
                robot.orientation.euler.yaw = euler[0]
                robot.orientation.euler.roll = euler[2]

        if owner_tracked and robot_tracked:
            owner_dist = sqrt(pow((owner.pos.x - robot.pos.x), 2) + pow((owner.pos.z - robot.pos.z), 2))
            owner_rot = owner.orientation.euler.yaw - robot.orientation.euler.yaw

            owner_dist_x = (owner.pos.x - robot.pos.x)
            owner_dist_z = (owner.pos.z - robot.pos.z)

            offset = 0.0
            rot = 0.0

            if owner_dist_z < 0:
                offset = 90
            elif owner_dist_z > 0:
                offset = - 90
            else:
                offset = 0

            p2p_rot_xz = atan(owner_dist_x / owner_dist_z) * 180 / pi

            rot = p2p_rot_xz - robot.orientation.euler.yaw + offset

            if rot > 180:
                rot = -360 + rot
            elif rot < -180:
                rot = 360 + rot


            rospy.loginfo("===================================================")
            #rospy.loginfo("owner_dist_x = %f", owner_dist_x)
            #rospy.loginfo("owner_dist_z = %f", owner_dist_z)
            #rospy.loginfo("owner_dist = %f", owner_dist)

            #rospy.loginfo("owner.orientation.euler.yaw = %f", owner.orientation.euler.yaw)
            rospy.loginfo("robot.orientation.euler.pitch = %f", robot.orientation.euler.pitch)
            rospy.loginfo("robot.orientation.euler.yaw = %f", robot.orientation.euler.yaw)
            rospy.loginfo("robot.orientation.euler.roll = %f", robot.orientation.euler.roll)
            #rospy.loginfo("offset = %f", offset)
            #rospy.loginfo("p2p_rot_xz = %f", p2p_rot_xz)
            rospy.loginfo("rot = %f", rot)

            if abs(rot) <= yaw_threshold:
                control_angular_vel = 0.0
                if owner_dist <= dist_thershold:
                    control_linear_x_vel = 0.0
                else:
                    control_linear_x_vel = 0.05
            elif rot < 0:
                control_angular_vel = -0.15
                control_linear_x_vel = 0.0
            elif rot > 0:
                control_angular_vel = 0.15
                control_linear_x_vel = 0.0
            else:
                rospy.loginfo("Undefined state")

        twist.linear.x = control_linear_x_vel
        twist.linear.y = control_linear_y_vel
        twist.linear.z = 0.0

        twist.angular.x = 0.0
        twist.angular.y = 0.0
        twist.angular.z = control_angular_vel

        pub.publish(twist)

        # print info to std out
        rospy.loginfo("Follow owner node  is running. count= %d", count)
        count += 1

        rate.sleep()
    rospy.loginfo("Follow owner node stopped.")


if __name__ == "__main__":
    rospy.init_node("follow_owner")
    follow_owner()
