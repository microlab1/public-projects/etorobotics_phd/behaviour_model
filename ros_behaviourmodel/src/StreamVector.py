
#!/usr/bin/env python3

class StreamVector_template:
    def __init__(self):
        self.data = []


class RigidBody:
    def __init__(self):
        self.name = ""
        self.id = 0
        self.tracked = True
        self.pos = self.PosVector()
        self.orientation = self.OriVector()

    class PosVector:
        def __init__(self):
            self.x = 0.0
            self.y = 0.0
            self.z = 0.0

    class OriVector:
        def __init__(self):
            self.euler = self.EulerVector()
            self.quat = self.QuatVector()

        class EulerVector:
            def __init__(self):
                self.roll = 0.0
                self.pitch = 0.0
                self.yaw = 0.0

        class QuatVector:
            def __init__(self):
                self.q1 = 0.0
                self.q2 = 0.0
                self.q3 = 0.0
                self.q4 = 0.0