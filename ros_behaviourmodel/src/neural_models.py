#!/usr/bin/env python3
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from tensorflow import keras

import rospy
from mecanum_led.srv import ControlLed
import mecanum_led.msg
import rospkg

class Neural:
    def __init__(self):
        rospack = rospkg.RosPack()
        path = rospack.get_path('ros_behaviourmodel')
        print("PATH: " + path)

        self.tail_model = keras.models.load_model(path + '/neural_models/tail_model/tail_model_weights.h5')
        self.attention_model = keras.models.load_model(path + '/neural_models/attention_model/attention_model_weights.h5')
        self.contact_model = keras.models.load_model(path + '/neural_models/contact_model/contact_model_weights.h5')

        self.tail_pred = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] # 20 measurement
        self.tail_wagging = False

        rospy.loginfo("Models loaded.")

        rospy.wait_for_service("control_led")
        self.client = rospy.ServiceProxy("control_led", ControlLed)

    def predict_tail_wagging(self, data):
        x_predict = [data.owner_dist, data.owner_rot, int(data.owner.tracked), int(data.owner_has_toy),
                      data.stranger_dist, data.stranger_rot, int(data.stranger.tracked), int(data.stranger_has_toy),
                      data.toy_dist, data.toy_rot, int(data.robot_has_toy), data.door_dist]

        y_predict = self.tail_model.predict_classes([x_predict])

        self.tail_pred.pop(0)
        self.tail_pred.append(y_predict)

        y_pred_av = sum(self.tail_pred) / len(self.tail_pred)

        if y_pred_av > 0.9:
            if not self.tail_wagging:
                color_on = mecanum_led.msg.Color(0, 0, 50)
                req = mecanum_led.srv.ControlLedRequest([color_on], 1, True, "blink")
                self.client(req)
                self.tail_wagging = True
        else:
            color_off = mecanum_led.msg.Color(0, 0, 0)
            req = mecanum_led.srv.ControlLedRequest([color_off], 1, False, "blink")
            self.client(req)
            self.tail_wagging = False
        return

    def predict_attention(self, data):
        x_predict = [data.owner_dist, data.owner_rot, int(data.owner.tracked), int(data.owner_has_toy),
                      data.stranger_dist, data.stranger_rot, int(data.stranger.tracked), int(data.stranger_has_toy),
                      data.toy_dist, data.toy_rot, int(data.robot_has_toy), data.door_dist]

        y_predict = self.attention_model.predict_classes([x_predict])
        return

    def predict_contact(self, data):
        x_predict = [data.owner_dist, data.owner_rot, int(data.owner.tracked), int(data.owner_has_toy),
                     data.stranger_dist, data.stranger_rot, int(data.stranger.tracked), int(data.stranger_has_toy),
                     data.toy_dist, data.toy_rot, int(data.robot_has_toy), data.door_dist]

        y_predict = self.contact_model.predict_classes([x_predict])
        return

    def calculate_predictions(self, data):
        self.predict_tail_wagging(data)
        self.predict_attention(data)
        self.predict_contact(data)
