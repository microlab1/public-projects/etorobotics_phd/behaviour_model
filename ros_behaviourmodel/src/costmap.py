#!/usr/bin/env python3

from math import *
import matplotlib.pyplot as plt

class Costmap:
    def __init__(self, r, resolution, mode):
        self.r = r
        self.resolution = resolution
        self.mode = mode

        self.modes = {0: "Idle",
                      1: "Prefer the closest object",
                      2: "Prefer the place, where the attraction score is the highest"}

        self.possible_positions = []
        self.costmap = []
        self.costmap_scores = []

    def set_range(self, r):
        self.r = r

    def set_resolution(self, resolution):
        self.resolution = resolution

    def set_mode(self, mode):
        self.mode = mode

    def get_modes(self):
        print(self.modes)

    def calculate_costmap(self, data, owner_attr, stranger_attr, toy_attr, door_attr):
        # helyek vizsgálata egy adott sugárban
        costmap_size = int(2 * (self.r / self.resolution)) + 1

        self.possible_positions = []

        for i in range(costmap_size):
            for j in range(costmap_size):
                possible_position = [data.robot.pos.z - self.r + j * self.resolution, data.robot.pos.x - self.r + i * self.resolution]
                self.possible_positions.append(possible_position)

        self.costmap = [[0 for i in range(costmap_size)] for j in range(costmap_size)]
        self.costmap_scores = []
        cost_x = 0
        cost_y = 0

        if self.mode == 0:
            for possible_position in self.possible_positions:
                d_owner = sqrt(pow(possible_position[0] - data.owner.pos.z, 2) +
                                    pow(possible_position[1] - data.owner.pos.x, 2))
                d_stranger = sqrt(pow(possible_position[0] - data.stranger.pos.z, 2) +
                                       pow(possible_position[1] - data.stranger.pos.x, 2))
                if d_owner == 0:
                    d_owner = d_owner + self.resolution
                if d_stranger == 0:
                    d_stranger = d_stranger + self.resolution
                if d_toy == 0:
                    d_toy = d_toy + self.resolution

                score = 1/d_owner + 1/d_stranger

                self.costmap[cost_x][cost_y] = score
                self.costmap_scores.append(score)

                cost_y = cost_y + 1
                if cost_y % costmap_size == 0:
                    cost_x = cost_x + 1
                    cost_y = 0

        elif self.mode == 1:
            for possible_position in self.possible_positions:
                d_owner = sqrt(pow(possible_position[0] - data.owner.pos.z, 2) +
                                    pow(possible_position[1] - data.owner.pos.x, 2))
                d_stranger = sqrt(pow(possible_position[0] - data.stranger.pos.z, 2) +
                                       pow(possible_position[1] - data.stranger.pos.x, 2))
                d_toy = sqrt(pow(possible_position[0] - data.toy.pos.z, 2) +
                                       pow(possible_position[1] - data.toy.pos.x, 2))
                d_door = sqrt(pow(possible_position[0] - data.door.pos.z, 2) +
                                        pow(possible_position[1] - data.door.pos.x, 2))

                if d_owner == 0:
                    d_owner = d_owner + self.resolution
                if d_stranger == 0:
                    d_stranger = d_stranger + self.resolution
                if d_toy == 0:
                    d_toy = d_toy + self.resolution
                if d_door == 0:
                    d_door = d_door + self.resolution

                owner_score = owner_attr / d_owner
                stranger_score = stranger_attr / d_stranger
                toy_score = toy_attr / d_toy
                door_score = door_attr / d_door
                score = owner_score + stranger_score + toy_score + door_score

                self.costmap[cost_x][cost_y] = score
                self.costmap_scores.append(score)
                cost_y = cost_y + 1
                if cost_y % costmap_size == 0:
                    cost_x = cost_x + 1
                    cost_y = 0

        return self.get_optimal_location()

    def get_optimal_location(self):
        max_value = max(self.costmap_scores)
        max_index = self.costmap_scores.index(max_value)
        print(self.possible_positions[max_index])
        return self.possible_positions[max_index]

    def plot_costmap(self):
        cm_image = plt.pcolormesh(self.costmap, cmap="plasma")
        plt.title("Attraction of possible positions")
        plt.xlabel("X element of costmap array")
        plt.ylabel("Y element of costmap array")
        plt.colorbar(cm_image)
        plt.show()
