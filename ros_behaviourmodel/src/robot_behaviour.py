#!/usr/bin/env python3

from random import *
from costmap import *


class RobotBehaviour:
    def __init__(self, type):
        ''' The variables could define what should the robot do:
            - playing
            - go to owner/stranger
            - sit in front of the door
            The variables could also have effect on the:
            - movement speed
            - tail wagging speed
            The variables are scaled between 0 and 1:
            - they can effect the other parameters easier (for example we can multiply the speed with the happiness)
            - they should increase or decrease with time

            There can be an easier behaviour model, where we exclude the effect of the behaviour variables and
            define the behaviour directly from the measurement data.
        '''
        self.type = type
        self.types = {0: "Update behaviour based on the position data",
                      1: "Update behaviour based on the attraction values of the objects.",
                      2: "Update behaviour based on the behaviour variables.",
                      66: 'Order 66, "follow" the owner.'}

        self.playfulness = 0.5
        self.trust_stranger = 0
        self.happiness = 0.5

        self.anxiety = 0
        self.exploration_level = 0.5
        self.missing_owner = 0

        self.robot_is_at_the_door = False
        self.robot_is_playing = False
        self.robot_is_exploring = False

        # threshold values
        self.door_dist_threshold = 0.5
        self.toy_rot_threshold = 10
        self.toy_dist_threshold = 2
        self.moving_threshold = 0.1

        self.owner_attr = 0.5
        self.stranger_attr = -0.5  # negative number means repel
        self.toy_attr = 0.5
        self.door_attr = 0.0

        self.dest = [0.0, 0.0]  # destination

        self.costmap = Costmap(2, 0.01, 1)

    def set_type(self, type):
        self.type = type

    def get_type(self):
        print(self.type)

    def get_types(self):
        print(self.types)

    def update_behaviour(self, data):
        if self.type == 0:
            self.update_based_on_data(data)
        elif self.type == 1:
            self.update_based_on_attractions(data)
            self.dest = self.costmap.calculate_costmap(data, self.owner_attr, self.stranger_attr, self.toy_attr,
                                                       self.door_attr)
        elif self.type == 2:
            self.update_based_on_variables(data)
        elif self.type == 66:
            self.order_66(data)
        else:
            print("Undefined behaviour model type.")
        return self.dest

    def update_based_on_data(self, data):
        if not data.owner.tracked and not data.stranger.tracked:
            self.wait_at_door(data)
            return
        else:
            self.robot_is_at_the_door = False

        if data.robot_has_toy:
            self.play()
            if data.owner.tracked:
                self.dest = [data.owner.pos.x, data.owner.pos.z]
            elif data.stranger.tracked:
                self.dest = [data.stranger.pos.x, data.stranger.pos.z]
            else:
                self.dest = [data.robot.pos.x, data.robot.pos.z]
            return
        elif data.owner_has_toy:
            self.play()
            if data.owner_dist > 0.6:
                self.dest = [data.owner.pos.x, data.owner.pos.z]
            else:
                self.dest = [data.robot.pos.x, data.robot.pos.z]
            return
        elif data.stranger_has_toy:
            self.play()
            if data.stranger_dist > 0.6:
                self.dest = [data.stranger.pos.x, data.stranger.pos.z]
            else:
                self.dest = [data.robot.pos.x, data.robot.pos.z]
            return
        elif data.toy_is_thrown:
            self.play()
            self.dest = [data.toy.pos.x, data.toy.pos.z]
            return
        elif data.toy_dist < self.toy_dist_threshold:
            self.play()
            self.dest = [data.toy.pos.x, data.toy.pos.z]
            return
        else:
            self.robot_is_playing = False

        if not self.robot_is_at_the_door and not self.robot_is_playing:
            self.explore(data)
            return
        else:
            self.robot_is_exploring = False
        return

    def update_based_on_attractions(self, data):
        self.owner_attr = 0.0
        self.stranger_attr = 0.0
        self.toy_attr = 0.0
        self.door_attr = 0.0

        if not data.owner.tracked and not data.stranger.tracked:
            self.door_attr = 0.5
            return

        if data.owner_has_toy:
            if data.owner_dist > 0.6:
                self.owner_attr = 0.5
            return
        if data.stranger_has_toy:
            if data.stranger_dist > 0.6:
                self.stranger_attr = 0.5
            return

        if data.robot_has_toy:
            self.owner_attr = 0.5
            self.stranger_attr = 0.5
            return
        else:
            if data.toy.tracked:
                self.toy_attr = 0.5
                return
            else:
                self.owner_attr = 0.5
                self.stranger_attr = 0.5
                return
        return

    def update_based_on_variables(self, data):
        if data.owner.tracked:
            if self.happiness < 1:
                self.happiness = self.happiness + 0.01
        else:
            if self.happiness > 0:
                self.happiness = self.happiness - 0.01
            print(self.happiness)
        return

    def order_66(self, data):
        self.dest = [data.owner.pos.x, data.owner.pos.z]
        return

    def wait_at_door(self, data):
        self.robot_is_at_the_door = True
        self.dest = [data.door.pos.x, data.door.pos.z]
        return

    def play(self):
        self.robot_is_playing = True
        return

    def explore(self, data):
        self.robot_is_exploring = True
        if data.owner.tracked:
            self.dest = [data.owner.pos.x, data.owner.pos.z]
        elif data.stranger.tracked:
            self.dest = [data.stranger.pos.x, data.stranger.pos.z]
        else:
            x = randint(0, 10) / 10
            z = randint(0, 10) / 10
            self.dest = [x, z]
        return

    def need_owner(self):
        print("go closer to the owner")
        print("if the owner is not in the room, sit in front of the door")
        return
