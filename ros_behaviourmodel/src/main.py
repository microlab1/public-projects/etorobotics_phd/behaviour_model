#!/usr/bin/env python3

from dataprocess import *
from costmap import *
from robot_behaviour import *
from neural_models import *


#from ros_behaviourmodel.msg import Positions

# TODO: wire in the neural networks -> they will also have effect on the behaviour
neural = Neural("tail")
neural.predict_tail_wagging([0])

# dummy code for testing
# it is similiar to the message, that will come from the mocapy stream
# initialize the positions
streamVector = StreamVector()

owner = RigidBody()
# Name
owner.name = "OWN"
# ID
owner.id = 0
# POS
owner.pos.x = 0.0
owner.pos.y = 0.0
owner.pos.z = 0.0
# Orientation
owner.orientation.euler.roll = 0.0
owner.orientation.euler.pitch = 0.0
owner.orientation.euler.yaw = 0.0
owner.orientation.quat.q1 = 1.0
owner.orientation.quat.q2 = 1.0
owner.orientation.quat.q3 = 1.0
owner.orientation.quat.q4 = 1.0

stranger = RigidBody()
# Name
stranger.name = "STR"
# ID
stranger.id = 0
# POS
stranger.pos.x = 0.0
stranger.pos.y = 0.0
stranger.pos.z = 0.0
# Orientation
stranger.orientation.euler.roll = 0.0
stranger.orientation.euler.pitch = 0.0
stranger.orientation.euler.yaw = 0.0
stranger.orientation.quat.q1 = 1.0
stranger.orientation.quat.q2 = 1.0
stranger.orientation.quat.q3 = 1.0
stranger.orientation.quat.q4 = 1.0

robot = RigidBody()
# Name
robot.name = "ROB"
# ID
robot.id = 0
# POS
robot.pos.x = 0.0
robot.pos.y = 0.0
robot.pos.z = 0.0
# Orientation
robot.orientation.euler.roll = 0.0
robot.orientation.euler.pitch = 0.0
robot.orientation.euler.yaw = 0.0
robot.orientation.quat.q1 = 1.0
robot.orientation.quat.q2 = 1.0
robot.orientation.quat.q3 = 1.0
robot.orientation.quat.q4 = 1.0

toy = RigidBody()
# Name
toy.name = "TOY"
# ID
toy.id = 0
# POS
toy.pos.x = 0.0
toy.pos.y = 0.0
toy.pos.z = 0.0
# Orientation
toy.orientation.euler.roll = 0.0
toy.orientation.euler.pitch = 0.0
toy.orientation.euler.yaw = 0.0
toy.orientation.quat.q1 = 1.0
toy.orientation.quat.q2 = 1.0
toy.orientation.quat.q3 = 1.0
toy.orientation.quat.q4 = 1.0

# Append to data
streamVector.data.append(owner)
streamVector.data.append(stranger)
streamVector.data.append(robot)
streamVector.data.append(toy)

for obj in streamVector.data:
    print(obj.name)

# call the dataProcessor and calculate the necessary data
data = dataProcessor_StreamVector(streamVector.data)

# calculate the new data
data.calculate_data(streamVector.data)

# initialize attraction values
attractions = Attractions()


# costmap related stuff
costmapCalc = Costmap(1, 0.1, 2)
costmapCalc.set_positions(data)
costmapCalc.get_modes()
costmapCalc.calculate_costmap(attractions)
costmapCalc.get_optimal_location()
#costmapCalc.plot_costmap()

# initialize state variables
robot_behaviour = RobotBehaviour(2)
robot_behaviour.get_types()

for i in range(10):
    robot_behaviour.update_behaviour(data,)
