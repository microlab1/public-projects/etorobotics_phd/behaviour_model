#!/usr/bin/env python3

from math import *
from time import *
from copy import *
from StreamVector import *


class dataProcessor_StreamVector:
    def __init__(self, streamVector_data):
        self.toy_threshold = 0.5

        self.mocapy_processed_elements = []
        self.mocapy_processed = []

        self.streamVector_data = deepcopy(streamVector_data)

        self.owner = RigidBody()
        self.stranger = RigidBody()
        self.robot = RigidBody()
        self.toy = RigidBody()
        self.door = RigidBody()

        self.prev_owner = RigidBody()
        self.prev_stranger = RigidBody()
        self.prev_robot = RigidBody()
        self.prev_toy = RigidBody()
        self.prev_door = RigidBody()

        self.owner_dist = 0
        self.stranger_dist = 0
        self.toy_dist = 0
        self.door_dist = 0

        self.owner_rot = 0
        self.stranger_rot = 0
        self.toy_rot = 0

        self.owner_toy_dist = 0
        self.stranger_toy_dist = 0
        self.robot_toy_dist = 0

        self.owner_has_toy = False
        self.stranger_has_toy = False
        self.robot_has_toy = False

        self.prev_owner_has_toy = False
        self.prev_stranger_has_toy = False
        self.prev_robot_has_toy = False

        self.toy_is_thrown = False
        self.grabber_closed = False

    def update_positions(self, streamVector_data):
        self.prev_owner = deepcopy(self.owner)
        self.prev_stranger = deepcopy(self.stranger)
        self.prev_robot = deepcopy(self.robot)
        self.prev_toy = deepcopy(self.toy)
        self.prev_door = deepcopy(self.door)

        self.prev_owner_has_toy = deepcopy(self.owner_has_toy)
        self.prev_stranger_has_toy = deepcopy(self.stranger_has_toy)
        self.prev_robot_has_toy = deepcopy(self.robot_has_toy)

        self.streamVector_data = deepcopy(streamVector_data)

    def update_distance(self):
        if self.robot.tracked:
            if self.owner.tracked:
                self.owner_dist = sqrt(pow((self.owner.pos.x - self.robot.pos.x), 2) + pow(
                    (self.owner.pos.z - self.robot.pos.z), 2))
            elif self.stranger.tracked:
                self.stranger_dist = sqrt(pow((self.stranger.pos.x - self.robot.pos.x), 2) + pow(
                    (self.stranger.pos.z - self.robot.pos.z), 2))
            elif self.toy.tracked:
                self.toy_dist = sqrt(pow((self.toy.pos.x - self.robot.pos.x), 2) + pow(
                    (self.toy.pos.z - self.robot.pos.z), 2))
            elif self.door.tracked:
                self.door_dist = sqrt(pow((self.door.pos.x - self.robot.pos.x), 2) + pow(
                    (self.door.pos.z - self.robot.pos.z), 2))

    def update_rotation(self):
        if self.robot.tracked:
            if self.owner.tracked:
                self.owner_rot = abs(self.owner.orientation.euler.yaw - self.robot.orientation.euler.yaw)
            elif self.stranger.tracked:
                self.stranger_rot = abs(self.stranger.orientation.euler.yaw - self.robot.orientation.euler.yaw)
            elif self.toy.tracked:
                self.toy_rot = abs(self.toy.orientation.euler.yaw - self.robot.orientation.euler.yaw)

    def update_has_toy(self):
        if not self.toy.tracked:
            return

        if self.owner_has_toy:
            if self.owner_toy_dist < self.toy_threshold:
                return
            else:
                self.owner_has_toy = False

        if self.stranger_has_toy:
            if self.stranger_toy_dist < self.toy_threshold:
                return
            else:
                self.stranger_has_toy = False

        if self.robot_has_toy:
            if self.robot_toy_dist < self.toy_threshold and self.grabber_closed:
                return
            else:
                self.robot_has_toy = False

        # if the toy is free
        if self.owner_toy_dist < self.toy_threshold:
            self.owner_has_toy = True
            return
        elif self.stranger_toy_dist < self.toy_threshold:
            self.stranger_has_toy = True
            return
        elif self.robot_toy_dist < self.toy_threshold:
            if self.grabber_closed:
                self.robot_has_toy = True
            return

    def distance_from_toy(self):
        self.owner_toy_dist = sqrt(pow((self.toy.pos.x - self.owner.pos.x), 2) + pow(
            (self.toy.pos.z - self.owner.pos.z), 2))
        self.stranger_toy_dist = sqrt(pow((self.toy.pos.x - self.stranger.pos.x), 2) + pow(
            (self.toy.pos.z - self.stranger.pos.z), 2))
        self.robot_toy_dist = sqrt(pow((self.toy.pos.x - self.robot.pos.x), 2) + pow(
            (self.toy.pos.z - self.robot.pos.z), 2))
        return

    def update_toy_is_thrown(self):
        if self.toy_is_thrown:
            if self.robot_has_toy:
                self.toy_is_thrown = False
                return

        if self.prev_owner_has_toy and not self.owner_has_toy:
            self.toy_is_thrown = True
            return
        elif self.prev_stranger_has_toy and not self.stranger_has_toy:
            self.toy_is_thrown = True
            return

    def calculate_data(self, streamVector_data):
        # start_time = time()

        self.update_positions(streamVector_data)

        for obj in streamVector_data:
            if obj.name == "OWN":
                self.owner = obj
            elif obj.name == "STR":
                self.stranger = obj
            elif obj.name == "ROB":
                self.robot = obj
            elif obj.name == "TOY":
                self.toy = obj
            elif obj.name == "DOOR":
                self.door = obj
            else:
                print("Unknown object.")

        self.update_has_toy()
        self.update_distance()
        self.update_rotation()
        self.update_toy_is_thrown()
        self.distance_from_toy()

        '''
        end_time = time()
        time_elapsed = end_time - start_time
        print("start time: ", start_time)
        print("end time: ", end_time)
        print("time elapsed: ", time_elapsed)
        '''
