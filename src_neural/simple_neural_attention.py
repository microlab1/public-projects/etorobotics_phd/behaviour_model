import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.callbacks import History, EarlyStopping
from os import listdir
from os.path import isfile, join
import matplotlib.pyplot as plt
from dataprocess import *


configuration = tf.compat.v1.ConfigProto()
configuration.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=configuration)

# loading the data from files and consturcting the myData EtoData instance
mocapy_files = []
solomon_files = []
onlyFiles = [f for f in listdir("../measurement/") if isfile(join("../measurement/", f))]
for file in onlyFiles:
    if file.endswith(".ecsv"):
        mocapy_files.append(file)
    elif file.endswith(".csv"):
        solomon_files.append(file)

cols = ["OWN_d", "OWN_r", "OWN.is_in_room", "OWN.has_toy", "STR_d", "STR_r",
        "STR.is_in_room", "STR.has_toy", "TOY_d", "TOY_r", "DOG.has_toy", "DOOR_d",
        "Attention", "Contact", "Tail"]

data_0 = EtoData(mocapy_files[0], solomon_files[0], cols)
data_1 = EtoData(mocapy_files[1], solomon_files[1], cols)
data_2 = EtoData(mocapy_files[2], solomon_files[2], cols)

data_0.separateData(0.7, 0.15)
data_1.separateData(0.7, 0.15)
data_2.separateData(0.7, 0.15)

train_data = np.concatenate((data_0.data_train, data_1.data_train, data_2.data_train))
val_data = np.concatenate((data_0.data_val, data_1.data_val, data_2.data_val))
test_data = np.concatenate((data_0.data_test, data_1.data_test, data_2.data_test))

# separating the inputs and outputs (outputs are int32, inputs are float32)
x_train = train_data[:, :-3].astype("float32")
y_train = train_data[:, -3].astype("int")
x_val = val_data[:, :-3].astype("float32")
y_val = val_data[:, -3].astype("int")
x_test = test_data[:, :-3].astype("float32")
y_test = test_data[:, -3].astype("int")

# structure of the neural network
model = keras.Sequential([
    keras.Input(shape=(x_train.shape[1])),
    layers.Dense(25, activation="relu"),
    layers.Dense(25, activation="relu"),
    layers.Dense(25, activation="relu"),
    layers.Dense(25, activation="relu"),
    layers.Dense(25, activation="relu")
])

print(model.summary())

#compiling the model
learning_rate = 0.0005
model.compile(
    loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    optimizer=keras.optimizers.Adam(lr=learning_rate),
    metrics=["accuracy"])
history = History()
earlyStop = EarlyStopping(monitor='val_accuracy', min_delta=0.0001, patience=10, verbose=1,
    mode='max', restore_best_weights=True)

# training the model
num_epochs = 50
model.fit(x_train, y_train, batch_size=32, epochs=num_epochs, validation_data=(x_val,y_val), verbose=2, callbacks=[history, earlyStop], shuffle=True)

# evaluate the model
model.evaluate(x_test, y_test, batch_size=32,verbose=2)

#model.save("model/attention_model")
#model.save("../ros_behaviourmodel/neural_models/attention_model")

model.save("model/attention_model/attention_model_weights.h5")
model.save("../ros_behaviourmodel/neural_models/attention_model/attention_model_weights.h5")

# for i in range(x_test.shape[0]):
#     y_pred = model.predict_classes(np.array([x_test[i,:]]))
#     if y_pred == [0]:
#         print(i)
#         print(y_pred)
#         print(y_test[i])

y_pred = np.argmax(model.predict(x_test), axis=-1)

plt.title("Attention")
plt.plot(y_pred, 'x', markersize=1, label="Attention prediction", color="blue")
plt.plot(y_test, ':', markersize=10, label="Attention", color="red")
plt.xlabel("Data number [-]")
plt.ylabel("Attention / 0: No attention 1: Look at owner 2: Look at stranger 3: Look at door 4: Look at toy")
plt.legend()
plt.show()

plt.plot(history.history["accuracy"], label="accuracy", color="blue")
plt.plot(history.history["val_accuracy"], label="accuracy of validation", color="red")
plt.title("Attention training accuracy")
plt.xlabel("Epoch number [-]")
plt.ylabel("Accuracy [-]")
plt.legend()
plt.show()
