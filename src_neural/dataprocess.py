import pandas as pd
import numpy as np
import math


class EtoData:
    def __init__(self, mocapy_file, solomon_file,cols):
        self.mocapy_file = "../measurement/" + mocapy_file
        self.solomon_file = "../measurement/" + solomon_file
        self.cols = cols

        # dictionaries of solomon behaviours
        self.scene_dict = {0.0: None, 0.5: "New Scene", 1.0: "Scene End"}
        self.tail_dict = {0: None, 1: "Tail wag"}
        self.attention_dict = {0: None, 1: "look at owner", 2: "look at stranger", 3: "look at door", 4: "look at toy"}
        self.contact_dict = {0: None, 1: "Contact with OWN", 2: "Contact with STR"}
        self.contact_seeking_dict = {0: None}

        # finding the header and config data in the mocapy file
        f = open(self.mocapy_file, "r")
        lines = f.readlines()
        f.close()
        hdr = lines.index("[MEASUREMENT]\n") + 1
        for i in range(hdr):
            if lines[i] == "\n":
                hdr -= 1

        # reading configuration data from the mocapy file
        cfg = lines.index("[CONFIG]\n")
        self.room_X = float(lines[cfg + 1].partition(":")[2])
        self.room_Y = float(lines[cfg + 2].partition(":")[2])
        self.room_Z = 2.0
        self.origin_offset_X = float(lines[cfg + 4].partition(":")[2])
        self.origin_offset_Y = float(lines[cfg + 5].partition(":")[2])

        # reading the raw data with pandas
        self.mocapy = pd.read_csv(self.mocapy_file, header=hdr)
        self.solomon = pd.read_csv(self.solomon_file, header=0, sep=";")

        # obtaining the raw data headers
        self.mhead = self.mocapy.columns.values.tolist()
        self.shead = self.solomon.columns.values.tolist()

        # extract scene start and endpoints from mocapy
        self.mocapy_scene_start_markers = []
        for i in range(len(self.mocapy)):
            if i > 1:
                if ((self.mocapy["Episode counter"][i] - self.mocapy["Episode counter"][i - 1]) != 0):
                    self.mocapy_scene_start_markers.append(i)
        self.mocapy_scene_start_markers = np.array(self.mocapy_scene_start_markers)
        self.mocapy_scene_end_markers = self.mocapy_scene_start_markers + 5999

        # extract scene start and endpoints from the solomon coder csv
        self.solomon_scene_start_markers = []  # list for scene startpoints
        self.solomon_scene_end_markers = []  # list for scene endpoints
        for i in range(len(self.solomon)):
            if self.solomon["Scene"][i] == "New Scene":
                self.solomon_scene_start_markers.append(i)
                self.solomon_scene_end_markers.append(
                    i + 5999)  # mocapy has preicsely 6000 datapoints / scene --> 6000 solomon points are extracted / scene
            # scene end markers are not used since the number of data points in solomon and in mocapy could be different
            # elif self.solomon["Scene"][i] == "Scene End":
            #    self.solomon_scene_end_markers.append(i)
        self.solomon_scene_start_markers = np.array(self.solomon_scene_start_markers)
        self.solomon_scene_end_markers = np.array(self.solomon_scene_end_markers)

        # data preprocessing

        # get the synchronised raw data
        self.mocapy_sync = self.mocapy.loc[self.mocapy_scene_start_markers[0]:].copy()
        self.solomon_sync = self.solomon.loc[self.solomon_scene_start_markers[0]:].copy()
        self.mocapy_sync.reset_index(inplace=True, drop=True)
        self.solomon_sync.reset_index(inplace=True, drop=True)

        # preprocessing the raw mocapy data (inputs)
        self.mocapy_preproc_head = self.mocapy_sync.columns.values.tolist()
        self.mocapy_preproc = self.mocapy_sync.to_numpy()
        self.mocapy_preproc[:, 1] = self.mocapy_preproc[:, 1] - self.mocapy_preproc[
            0, 1]  # start counting time from the beginning of synced data

        delind = [0]  # deleting field: Frame
        self.mocapy_preproc = np.delete(self.mocapy_preproc, delind, axis=1)  # delete the unneeded columns
        delind.sort(reverse=True)
        for i in delind:
            self.mocapy_preproc_head.pop(i)  # deleting the unwanted column names
        self.mocapy_preproc = np.float32(self.mocapy_preproc)  # convert every value to float32

        self.add_distance("OWN")  # add OWNer's distance (measured from DOG)
        self.add_rotation("OWN")  # add OWNer's relative normalised rotaion (measured from DOG)
        self.add_distance("OHA")  # add Owner's HAnd's distance (measured from DOG)
        self.add_rotation("OHA")  # add Owner's HAnd's relative normalised rotaion (measured from DOG)
        self.add_distance("STR")  # add STRanger's distance (measured from DOG)
        self.add_rotation("STR")  # add STRanger's relative normalised rotation (measured from DOG)
        self.add_distance("SHA")  # add Stranger's HAnd's distance (measured from DOG)
        self.add_rotation("SHA")  # add Stranger's HAnd's relative normalised rotation (measured from DOG)
        self.add_distance("TOY")  # add TOY's relative distance (measured from DOG)
        self.add_rotation("TOY")  # add TOY's relative normalised rotation (measured from DOG)
        self.add_distance("DOOR")  # add DOOR's relative distance (measured from DOG)
        self.add_rotation("DOOR")  # add DOOR's relative normalised rotation (measured from DOG)
        self.add_movementDistance("OWN")
        self.add_movementDistance("STR")
        self.add_movementDistance("DOOR")

        # preprocessing the raw solomon data (tags)
        # first the strings have to be replaced with numbers according to the behaviour dictionaries
        self.solomon_sync["Scene"] = self.solomon_sync["Scene"].replace(self.scene_dict.values(),
                                                                        self.scene_dict.keys())
        self.solomon_sync["Attention"] = self.solomon_sync["Attention"].replace(self.attention_dict.values(),
                                                                                self.attention_dict.keys())
        self.solomon_sync["Contact"] = self.solomon_sync["Contact"].replace(self.contact_dict.values(),
                                                                            self.contact_dict.keys())
        self.solomon_sync["Contact Seeking"] = self.solomon_sync["Contact Seeking"].replace(
            self.contact_seeking_dict.values(), self.contact_seeking_dict.keys())
        self.solomon_sync["Tail"] = self.solomon_sync["Tail"].replace(self.tail_dict.values(), self.tail_dict.keys())

        self.solomon_preproc = self.solomon_sync.to_numpy()  # pandas frame to numpy array

        self.solomon_preproc_head = self.solomon_sync.columns.values.tolist()  # pandas columns to list
        delind = [1]  # index of unneeded columns to be deleted
        self.solomon_preproc = np.delete(self.solomon_preproc, delind, axis=1)  # delete the unneeded columns
        delind.sort(reverse=True)
        for i in delind:
            self.solomon_preproc_head.pop(i)  # deleting the unwanted column names

        for i in range(self.solomon_preproc.shape[0]):
            self.solomon_preproc[i][0] = self.solomon_preproc[i][0].replace(",", ".")
        self.solomon_preproc = np.float32(self.solomon_preproc) # convert every value to float32
        solomon_start = self.solomon_preproc[0][0]
        for i in range(self.solomon_preproc.shape[0]):
            self.solomon_preproc[i][0] = round(self.solomon_preproc[i][0] - solomon_start, 1)

        for i in range(self.mocapy_preproc.shape[0]):
            if self.solomon_preproc[self.solomon_preproc.shape[0]-1][0] <= round(self.mocapy_preproc[i][0],1):
                self.mocapy_preproc = self.mocapy_preproc[:i,:]
                break

        self.solomon_preproc_extended = np.empty((self.mocapy_preproc.shape[0], 5), float)
        last_index = 0
        # concat solomon and mocapy measurements

        for i in range(self.mocapy_preproc.shape[0]):
            for j in range(self.solomon_preproc.shape[0]):
                j += last_index
                if abs(round(self.mocapy_preproc[i][0],1) - self.solomon_preproc[j][0]) < 0.001:
                    self.solomon_preproc_extended[i] = self.solomon_preproc[j]
                    last_index = j
                    break

        # end of data preprocessing

        # preprocess scene start and end markers
        self.preproc_scene_start = self.mocapy_scene_start_markers - self.mocapy_scene_start_markers[0]
        self.preproc_scene_end = self.mocapy_scene_end_markers - self.mocapy_scene_start_markers[0] + 1

        self.data_head = self.cols
        self.data = self.get_preproc_cols(col_names=self.cols)
        self.data_train = np.array([]).reshape((-1, self.data.shape[1]))
        self.data_val = np.array([]).reshape((-1, self.data.shape[1]))
        self.data_test = np.array([]).reshape((-1, self.data.shape[1]))

    def add_distance(self, obj):
        d = np.concatenate(
            [(self.get_preproc_cols([obj + ".position.x"]) - self.get_preproc_cols(["DOG.position.x"])),
             (self.get_preproc_cols([obj + ".position.y"]) - self.get_preproc_cols(["DOG.position.y"]))],
            axis=1)
        d = np.sum(np.square(d), axis=1).reshape((-1, 1))
        d = np.sqrt(d)
        self.mocapy_preproc = np.append(self.mocapy_preproc, d, axis=1)
        self.mocapy_preproc_head.append(obj + "_d")

    # nested function to add object's relative rotation (measured from DOG) to mocapy_preproc, call with object's name
    # this rotation value is normalised
    def add_rotation(self, obj):
        r_array = abs(np.add(self.get_preproc_cols([obj + ".rot_Z"]), 180) - np.add(self.get_preproc_cols(["DOG.rot_Z"]), 180))
        for i in range(len(r_array)):
            if r_array[i] > 180:
                r_array[i] = 360-r_array[i]
        self.mocapy_preproc = np.append(self.mocapy_preproc, r_array, axis=1)
        self.mocapy_preproc_head.append(obj + "_r")

    def add_movementDistance(self, obj):
        d = np.concatenate(
            [(self.get_preproc_cols([obj + ".position.x"])),
             (self.get_preproc_cols([obj + ".position.y"]))],
            axis=1)
        movement_distance = []
        for i in range(self.mocapy_preproc.shape[0]):
            if i == 0:
                movement_distance.append(0)
            else:
                movement_distance.append(math.sqrt(math.pow(self.mocapy_preproc[i][0], 2) +
                                                   math.pow(self.mocapy_preproc[i][1], 2)) -
                                         math.sqrt(math.pow(self.mocapy_preproc[i-1][0], 2) +
                                                   math.pow(self.mocapy_preproc[i-1][1], 2)))
        movement_distance = np.array(movement_distance)
        movement_distance_formatted = movement_distance.reshape(-1, 1)
        self.mocapy_preproc = np.append(self.mocapy_preproc, movement_distance_formatted, axis=1)
        self.mocapy_preproc_head.append(obj + "_movement_distance")

    # this function returns the normalised data specified by the colnames "names"
    def get_preproc_cols(self, col_names):
        outs = []
        for name in col_names:
            if name in self.mocapy_preproc_head:
                outs.append(self.mocapy_preproc[:, self.mocapy_preproc_head.index(name)])
            elif name in self.solomon_preproc_head:
                outs.append(self.solomon_preproc_extended[:, self.solomon_preproc_head.index(name)])
        out = outs[0].reshape((-1, 1))
        for i in range(1, len(outs)):
            out = np.append(out, outs[i].reshape((-1, 1)), axis=1)
        return out

    def keepData(self, start, end):
        '''keep the desired amount of data.'''
        self.data = self.data[start:end, :]

    def separateData(self, train_part, val_part):
        num_divs = 70

        # data_0
        divlen = int(np.floor(len(self.data) / num_divs))
        train_len = int(np.floor(train_part * divlen))
        val_len = int(np.floor(val_part * divlen))

        for i in range(num_divs):
            if i == (num_divs - 1):
                div = self.data[(i * divlen):, :]
            else:
                div = self.data[(i * divlen):((i + 1) * divlen), :]
            self.data_train = np.append(self.data_train, div[0:train_len, :], axis=0)
            self.data_val = np.append(self.data_val, div[train_len:(train_len + val_len), :], axis=0)
            self.data_test = np.append(self.data_test, div[(train_len + val_len):len(div), :], axis=0)
