
from os import listdir
from os.path import isfile, join

import matplotlib.pyplot as plt
import numpy as np

from dataprocess import *


def time_duration(x, y, value):
    active = False
    tmp_start_time = 0
    tmp_end_time = 0
    t_dur = []

    for i in range(len(y)):
        if not active and y[i] == value:
            active = True
            tmp_start_time = x[i]
            tmp_end_time = 0
        elif active and y[i] != value:
            tmp_end_time = x[i]
            time = tmp_end_time - tmp_start_time
            t_dur.append(time)
            active = False

    np_time_duration = np.array(t_dur)
    if np_time_duration.size == 0:
        print("empty")
        return
    #print(np_time_duration)
    print(np_time_duration.mean())
    print(np_time_duration.min())
    print(np_time_duration.max())

    return np_time_duration

# loading the data from files and consturcting the myData EtoData instance
mocapy_files = []
solomon_files = []
onlyfiles = [f for f in listdir("../measurement/") if isfile(join("../measurement/", f))]
for file in onlyfiles:
    if file.endswith(".ecsv"):
        mocapy_files.append(file)
    elif file.endswith(".csv"):
        solomon_files.append(file)
cols = ["Time", "DOG.has_toy", "Attention", "Contact", "Tail"]
train_val_Data0 = EtoData(mocapy_files[0], solomon_files[0],cols)
train_val_Data1 = EtoData(mocapy_files[1], solomon_files[1],cols)
test_Data = EtoData(mocapy_files[2], solomon_files[2],cols)

# print(train_val_Data0.data_head)
# print(train_val_Data0.data.shape)
# print(train_val_Data1.data_head)
# print(train_val_Data1.data.shape)
# print(test_Data.data_head)
# print(test_Data.data.shape)

x = train_val_Data0.data[:,0]
x_1 = train_val_Data1.data[:,0]
x_2 = test_Data.data[:,0]

y_tail = train_val_Data0.data[:,-1]
y_tail_1 = train_val_Data1.data[:,-1]
y_tail_2 = test_Data.data[:,-1]

y_contact = train_val_Data0.data[:,-2]
y_contact_1 = train_val_Data1.data[:,-2]
y_contact_2 = test_Data.data[:,-2]

y_attention = train_val_Data0.data[:,-3]
y_attention_1 = train_val_Data1.data[:,-3]
y_attention_2 = test_Data.data[:,-3]

y_has_toy = train_val_Data0.data[:,-4]
y_has_toy_1 = train_val_Data1.data[:,-4]
y_has_toy_2 = test_Data.data[:,-4]

print("Tail")
np_tail_duration = time_duration(x, y_tail, 1)
np_tail_duration_1 = time_duration(x_1, y_tail_1, 1)
np_tail_duration_2 = time_duration(x_2, y_tail_2, 1)

print("Contact")
for i in range(2):
    print(i + 1)
    np_contact_duration = time_duration(x, y_contact, i + 1)
    np_contact_duration_1 = time_duration(x_1, y_contact_1, i + 1)
    np_contact_duration_2 = time_duration(x_2, y_contact_2, i + 1)

print("Attention")
for j in range(4):
    print(j + 1)
    np_attention_duration = time_duration(x, y_attention, j + 1)
    np_attention_duration_1 = time_duration(x_1, y_attention_1, j + 1)
    np_attention_duration_2 = time_duration(x_2, y_attention_2, j + 1)

print("Dog has toy")
np_dog_has_toy_duration = time_duration(x, y_has_toy, 1)
np_dog_has_toy_duration_1 = time_duration(x_1, y_has_toy_1, 1)
np_dog_has_toy_duration_2 = time_duration(x_2, y_has_toy_2, 1)

fig, axs = plt.subplots(3, sharex=True)
fig.suptitle('Tail wagging')
axs[0].plot(x, y_tail, label="0: not wagging 1: wagging")
axs[0].set(title="1st measurement")
axs[0].set(ylabel="Tail wagging")
axs[0].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[0].set(yticks=np.arange(0, max(y_tail_2)+0.1, 1))
axs[0].legend()
axs[1].plot(x_1, y_tail_1)
axs[1].set(title="2nd measurement")
axs[1].set(ylabel="Tail wagging")
axs[1].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[1].set(yticks=np.arange(0, max(y_tail_2)+0.1, 1))
axs[2].plot(x_2, y_tail_2)
axs[2].set(title="3rd measurement")
axs[2].set(ylabel="Tail wagging")
axs[2].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[2].set(yticks=np.arange(0, max(y_tail_2)+0.1, 1))
plt.xlabel("Time [s]")
plt.show()

fig, axs = plt.subplots(3, sharex=True, sharey=True)
fig.suptitle('Contact')
axs[0].plot(x, y_contact, label="0: No contact 1: Contact with owner 2: Contact with stranger")
axs[0].set(title="1st measurement")
axs[0].set(ylabel="Contact")
axs[0].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[0].set(yticks=np.arange(0, max(y_contact_2)+0.1, 1))
axs[0].legend()
axs[1].plot(x_1, y_contact_1)
axs[1].set(title="2nd measurement")
axs[1].set(ylabel="Contact")
axs[1].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[1].set(yticks=np.arange(0, max(y_contact_2)+0.1, 1))
axs[2].plot(x_2, y_contact_2)
axs[2].set(title="3rd measurement")
axs[2].set(ylabel="Contact")
axs[2].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[2].set(yticks=np.arange(0, max(y_contact_2)+0.1, 1))
plt.xlabel("Time [s]")
plt.show()

fig, axs = plt.subplots(3, sharex=True, sharey=True)
fig.suptitle('Attention')
axs[0].plot(x, y_attention, label="0: No attention 1: Look at owner 2: Look at stranger 3: Look at door 4: Look at toy")
axs[0].set(title="1st measurement")
axs[0].set(ylabel="Attention")
axs[0].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[0].set(yticks=np.arange(0, max(y_attention_2)+0.1, 1))
axs[0].legend()
axs[1].plot(x_1, y_attention_1)
axs[1].set(title="2nd measurement")
axs[1].set(ylabel="Attention")
axs[1].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[1].set(yticks=np.arange(0, max(y_attention_2)+0.1, 1))
axs[2].plot(x_2, y_attention_2)
axs[2].set(title="3rd measurement")
axs[2].set(ylabel="Attention")
axs[2].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[2].set(yticks=np.arange(0, max(y_attention_2)+0.1, 1))
plt.xlabel("Time [s]")
plt.show()

fig, axs = plt.subplots(3, sharex=True, sharey=True)
fig.suptitle('DOG has toy')
axs[0].plot(x, y_has_toy, label="0: Dog not has toy 1: Dog has toy")
axs[0].set(title="1st measurement")
axs[0].set(ylabel="Dog has toy")
axs[0].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[0].set(yticks=np.arange(0, max(y_has_toy_2)+0.1, 1))
axs[0].legend(fontsize=10)
axs[1].plot(x_1, y_has_toy_1)
axs[1].set(title="2nd measurement")
axs[1].set(ylabel="Dog has toy")
axs[1].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[1].set(yticks=np.arange(0, max(y_has_toy_2)+0.1, 1))
axs[2].plot(x_2, y_has_toy_2)
axs[2].set(title="3rd measurement")
axs[2].set(ylabel="Dog has toy")
axs[2].set(xticks=np.arange(0, max(x_2)+1, 60))
axs[2].set(yticks=np.arange(0, max(y_has_toy_2)+0.1, 1))
plt.xlabel("Time [s]")
plt.show()

#plt.savefig('figures\Tail_wagging.png')

'''
Tail:
    First video
 - average duration:
 - min:
 - max:
 
    Second video
 - average duration:
 - min:
 - max:
 
    Third video
 - average duration:
 - min:
 - max:
 
Contact:
    First video
 - average duration:
 - min:
 - max:
 
    Second video
 - average duration:
 - min:
 - max:
 
    Third video
 - average duration:
 - min:
 - max:
 
Attention:
    First video
 - average duration:
 - min:
 - max:
 
    Second video
 - average duration:
 - min:
 - max:
 
    Third video
 - average duration:
 - min:
 - max:
 
'''
