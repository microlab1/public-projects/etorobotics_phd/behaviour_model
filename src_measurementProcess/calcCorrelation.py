
from os import listdir
from os.path import isfile, join

import matplotlib.pyplot as plt

from dataprocess import *

# loading the data from files and consturcting the myData EtoData instance
mocapy_files = []
solomon_files = []
onlyfiles = [f for f in listdir("../measurement/") if isfile(join("../measurement/", f))]
for file in onlyfiles:
    if file.endswith(".ecsv"):
        mocapy_files.append(file)
    elif file.endswith(".csv"):
        solomon_files.append(file)

cols = ["OWN_d", "OWN_r", "OHA_d", "OHA_r", "OWN.is_in_room", "OWN.has_toy", "STR_d", "STR_r", "SHA_d",
        "SHA_r", "STR.is_in_room", "STR.has_toy", "TOY_d", "TOY_r", "DOG.has_toy", "DOOR_d", "DOOR_r",
        "OWN_moved_d", "STR_moved_d", "DOOR_moved_d",
        "Attention", "Contact", "Tail"]

train_val_Data0 = EtoData(mocapy_files[0], solomon_files[0], cols)
train_val_Data1 = EtoData(mocapy_files[1], solomon_files[1], cols)
test_Data = EtoData(mocapy_files[2], solomon_files[2], cols)

tmp_data = np.concatenate((train_val_Data0.data, train_val_Data1.data, test_Data.data))

# tmp_data = np.append([[train_val_Data0.data], [train_val_Data1.data], [test_Data.data]], axis=1)

# print(np.corrcoef(tmp_data[:, -1], tmp_data[:, -11]))

data = pd.DataFrame(data=tmp_data, columns=cols)    # , index=["row1", "row2"], columns=["column1", "column2"]

corr = data.corr(method='pearson')
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(corr, cmap='coolwarm', vmin=-1, vmax=1)
fig.colorbar(cax)
ticks = np.arange(0, len(data.columns), 1)
ax.set_xticks(ticks)
plt.xticks(rotation=90)
ax.set_yticks(ticks)
ax.set_xticklabels(data.columns)
ax.set_yticklabels(data.columns)
plt.show()
